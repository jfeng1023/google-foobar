Problem Description
=====================

You need to pass a message to the bunny prisoners, but to avoid detection, the code you agreed to use is... obscure, to say the least. The bunnies are given food on standard-issue prison plates that are stamped with the numbers 0-9 for easier sorting, and you need to combine sets of plates to create the numbers in the code. The signal that a number is part of the code is that it is divisible by 3. You can do smaller numbers like 15 and 45 easily, but bigger numbers like 144 and 414 are a little trickier. Write a program to help yourself quickly create large numbers for use in the code, given a limited number of plates to work with.

You have L, a list containing some digits (0 to 9). Write a function answer(L) which finds the largest number that can be made from some or all of these digits and is divisible by 3. If it is not possible to make such a number, return 0 as the answer. L will contain anywhere from 1 to 9 digits.  The same digit may appear multiple times in the list, but each element in the list may only be used once.


Examples
==========

Inputs:
    (int list) l = [3, 1, 4, 1]
    
Output:
    (int) 4311

Inputs:
    (int list) l = [3, 1, 4, 1, 5, 9]
    
Output:
    (int) 94311
    

Explanation of My Approach
============================

A number is divisible by 3 if and only if the sum of its digits is divisible by 3. If sum of digits mod 3 is 1 or 2, the number itself mod 3 is also 1 or 2.

If the sum of given digits is not divisible by 3, we first try to remove as few digits as necessary, and then try to remove the smallest digits.

The ten digits can be divided into 3 groups:

Group 1 (x%3 = 1): 1, 4, 7

Group 2 (x%3 = 2): 2, 5, 8

Group 3 (x%3 = 0): 0, 3, 6, 9

Given a list of digits, L, if sum(L)%3 = 1, the remainder is caused by either one digit from group 1, or two digits from group 2; if sum(L)%3 = 2, the remainder is caused by either one digit from group 2, or two digits from group 1. So in either case, we first try to remove one digit from the appropriate group (1 or 2); if there's no digit from that group, we remove two digits from the other group (2 or 1). This is guaranteed to work, except when we're only given one or two digits and they're all removed.