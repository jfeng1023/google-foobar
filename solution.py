# Given a sorted list of numbers and the remainder when their sum is divided by 3, remove as few numbers as possible to make the remainder 0
def remove_remainder(nums, remainder):
    rems = [n%3 for n in nums]
    if remainder == 1:
        first, second = 1,2
    else:
        first, second = 2,1
    try:
        index_to_remove = rems.index(first)
        nums.pop(index_to_remove)
    except ValueError:
        first_index = rems.index(second)
        rems.pop(first_index)
        nums.pop(first_index)
        second_index = rems.index(second)
        nums.pop(second_index)
    return nums
    

def answer(L):
    nums = sorted(L)
    # if every number is 0, return 0 immediately
    if nums[-1] == 0:
        return 0
    else:
        remainder = sum(nums)%3
        if remainder == 0:
            return ''.join([str(n) for n in reversed(nums)])
        else:
            nums_left = remove_remainder(nums, remainder)
            return 0 if len(nums_left) == 0 else ''.join([str(n) for n in reversed(nums_left)])

                
